colSds <- function(x, na.rm = FALSE) apply(X = x, MARGIN = 2, FUN = sd, na.rm = na.rm)

createNormalizer <- function(data) {
    means <- colMeans(data, na.rm = TRUE)
    sds <- colSds(data, na.rm = TRUE)

    normalizer <- list(Means = means, StandardDeviations = sds)
    class(normalizer) <- append(x = class(normalizer), values = 'Normalizer')
    return(normalizer)
}


# data = data.frame(XX = 1:10, YY = 11:20)
